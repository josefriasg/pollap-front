import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with basic auth credentials if available
        let jsonData = sessionStorage.getItem('data')
        if (jsonData){
            let data = JSON.parse(jsonData);
            let token = data.token;
            if (token) {
                //return next.handle(req.clone({headers: req.headers.append('Authorization', 'Bearer ' + token)
                //httpOptions.headers = httpOptions.headers.set('Authorization', 'my-new-auth-token');
                /*request = request.clone({
                    setHeaders: { 
                        Authorization: 'Basic ${token}'
                    }
                });*/
                const authRequest = request.clone({
                    headers: request.headers.set('Authorization', 'Bearer '+token)
                });
                return next.handle(authRequest);
            }else{
                return next.handle(request);
            }
        }else{
            return next.handle(request);
        }
    }
}