import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpResponse,
    HttpErrorResponse
   } from '@angular/common/http';
   import { Observable, throwError } from 'rxjs';
   import { retry, catchError } from 'rxjs/operators';
   import {AlertsService} from '../../services/alerts.service';
   import { Injectable } from "@angular/core"
   import { Router } from '@angular/router';
  
   @Injectable()
   export class HttpErrorInterceptor implements HttpInterceptor {

    constructor(private alertsService: AlertsService, private router: Router) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      return next.handle(request)
        .pipe(
          retry(1),
          catchError((error: HttpErrorResponse) => {
            let errorMessage = '';
            let errorMessageUsuario='Error ejecutando la operación';
            if (error.error instanceof ErrorEvent) {
              // client-side error
              errorMessage = 'Event error:'+error.error.message;
            } else {
              // server-side error
              errorMessage =  'Status:'+error.status +'\nMessage:'+ error.message;
            }
            //window.alert(errorMessage);
            this.alertsService.alertErrorMessage(errorMessageUsuario);

            if (error.status === 401) {
              alert('La sesión se ha cerrado. Por favor vuelve a ingresar');
              this.router.navigate(['/login']);
          }
          
            return throwError(errorMessage);
          })
        )
    }
   }