import { Component, OnInit } from '@angular/core';

import {debounceTime} from 'rxjs/operators';
import {AlertsService} from '../../../services/alerts.service';

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.scss']
})
export class AlertsComponent implements OnInit {
  
  successMessage: string;
  errorMessage: string;

  constructor(private alertService: AlertsService) { }

  ngOnInit() {

    this.alertService.getSuccessAlert().subscribe((message) => this.successMessage = message);
    this.alertService.getErrorAlert().subscribe((message) => this.errorMessage = message);

    this.alertService.getSuccessAlert().pipe(
      debounceTime(5000)
    ).subscribe(() => this.successMessage = null);

    this.alertService.getErrorAlert().pipe(
      debounceTime(5000)
    ).subscribe(() => this.errorMessage = null);

  }
}
