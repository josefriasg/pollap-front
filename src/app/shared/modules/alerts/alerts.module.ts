import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AlertsComponent } from './alerts.component';

@NgModule({
  imports: [CommonModule, NgbModule],
  declarations: [AlertsComponent],
  exports: [AlertsComponent],
  bootstrap: [AlertsComponent]
})
export class AlertsModule { }
