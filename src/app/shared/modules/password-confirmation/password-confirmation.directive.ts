import { Directive } from '@angular/core';
import { AbstractControl, FormGroup, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';

export const passwordConfirmationValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const password = control.get('password');
  const confPassword = control.get('confPassword');

  return password && confPassword && password.value != confPassword.value ? { 'passwordsMismatch': true } : null;
};

@Directive({
  selector: '[appPasswordConfirmation]',
  providers: [{ provide: NG_VALIDATORS, useExisting: PasswordConfirmationDirective, multi: true }]
})
export class PasswordConfirmationDirective implements Validator{

  constructor() { }

  validate(control: AbstractControl): ValidationErrors {
    return passwordConfirmationValidator(control)
  }

}
