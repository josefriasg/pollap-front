import { NgModule } from '@angular/core';
import { PasswordConfirmationDirective } from './password-confirmation.directive';

@NgModule({
  declarations: [PasswordConfirmationDirective],
  exports: [
    PasswordConfirmationDirective
]
})
export class PasswordConfirmationModule { }
