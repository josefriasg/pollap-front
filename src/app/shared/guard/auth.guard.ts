import { Injectable, Inject } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router, @Inject(DOCUMENT) private document: Document) {}

    canActivate() {
        if (this.document.location.href.indexOf("#/sso/")>0){
            sessionStorage.removeItem('data');
            localStorage.removeItem('isLoggedin');
            localStorage.removeItem('picture');
            sessionStorage.setItem('ticket', this.document.location.href.substring(this.document.location.href.indexOf("#/sso/")+6));
            this.router.navigate(['/sso']);
            return false;
        }else if(this.document.location.href.indexOf("#/activationError")>0){
            sessionStorage.removeItem('data');
            localStorage.removeItem('isLoggedin');
            localStorage.removeItem('picture');
            this.router.navigate(['/activationError']);
            return false;
        }else if(this.document.location.href.indexOf("#/activationSuccess")>0){
            sessionStorage.removeItem('data');
            localStorage.removeItem('isLoggedin');
            localStorage.removeItem('picture');
            this.router.navigate(['/activationSuccess']);
            return false;
        }

        if (sessionStorage.getItem('data')==null){
            this.router.navigate(['/login']);
            return false;
        }

        if (localStorage.getItem('isLoggedin')) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }
}
