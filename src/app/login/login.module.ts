import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { LoginService } from '../services/login.service';
import { UsuarioService } from '../services/usuario.service';
import { AlertsModule } from '../shared/modules/alerts/alerts.module';

@NgModule({
    imports: [CommonModule, LoginRoutingModule, FormsModule, NgbModule.forRoot(), AlertsModule],
    declarations: [LoginComponent],
  providers: [
      LoginService, UsuarioService
    ]
})
export class LoginModule {}
