import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { Observable, of } from 'rxjs';
import { LoginService } from '../services/login.service';
import { UsuarioService } from '../services/usuario.service';
import { AlertsService } from '../services/alerts.service';
import { PictureService } from '../services/picture.service';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    @Input() model: any = {};
    @Input() email:string;
    constructor(public router: Router, private loginService: LoginService, private usuarioService: UsuarioService,private alertService: AlertsService, private modalService: NgbModal, private pictureService:PictureService) {}

    ngOnInit() {
        sessionStorage.removeItem('data');
        localStorage.removeItem('isLoggedin');
    }

    onLoggedin() {
        //localStorage.setItem('isLoggedin', 'true');
        sessionStorage.removeItem('data');
        localStorage.removeItem('isLoggedin');
        this.loginService.login(this.model.username, this.model.password).subscribe(data => {
            if (data != null) {
                if (data.token){
                    sessionStorage.setItem(
                    'data',JSON.stringify(data)
                    //btoa(this.model.username + ':' + this.model.password)
                    );

                    //localStorage.removeItem('picture');
                    localStorage.setItem('isLoggedin', 'true');
                    
                    this.usuarioService.getUserWithProfilePicture(this.model.username).subscribe(user =>{
                        localStorage.setItem('picture', user.picture);
                        this.pictureService.updatePicture(user.picture);
                        this.router.navigate(['dashboard']);
                    })
                    
                }else{
                    sessionStorage.removeItem('data');
                    localStorage.removeItem('isLoggedin');
                    this.alertService.alertErrorMessage('Error durante la autenticación');
                    //alert("Authentication failed.");
                    this.router.navigate(['login']);
                }
            } else {
                sessionStorage.removeItem('data');
                localStorage.removeItem('isLoggedin');
                this.alertService.alertErrorMessage('Error durante la autenticación');
                //alert("Authentication failed.");
                this.router.navigate(['login']);
            }
        });
    }

    forgotPassword(content : any):void{
        this.email='';
        this.modalService.open(content, { size: 'lg', centered : true });
      }

      onSubmit(c:any){
        this.loginService.resetPassword(this.email).subscribe(result => this.afterSave(c, result));
    }

    afterSave(c:any, result:boolean){
        c('Saved');
        if (result){
          this.alertService.alertSuccessMessage('Nueva contraseña enviada a su correo');
        }else{
          this.alertService.alertErrorMessage('No se pudo enviar la nueva contraseña');
        }
        
      }
}
