import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';

@Component({
  selector: 'app-activation-success',
  templateUrl: './activation-success.component.html',
  styleUrls: ['./activation-success.component.scss'],
  animations: [routerTransition()]
})
export class ActivationSuccessComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
