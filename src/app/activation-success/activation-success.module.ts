import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivationSuccessComponent } from './activation-success.component';
import { ActivationSuccessRoutingModule } from './activation-success-routing.module';

@NgModule({
  declarations: [ActivationSuccessComponent],
  imports: [
    CommonModule,
    ActivationSuccessRoutingModule
  ]
})
export class ActivationSuccessModule { }
