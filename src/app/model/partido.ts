import { Torneo } from "./torneo";
import { Equipo } from "./equipo";

export class Partido {
    idPartido: number;
    equipo1: Equipo;
    equipo2: Equipo;
    torneo: Torneo;
    resultadoFinalEquipo1 : number;
    resultadoFinalEquipo2 : number;
    inicio: Date;
    esFinal : boolean;
    externalId: number;
    //ronda : string ???

  }