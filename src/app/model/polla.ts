import { Usuario } from "./usuario";
import { Torneo } from "./torneo";

export class Polla {
    idPolla: number;
    name: string;
    torneo: Torneo
    creador: Usuario;
    emailsInvitados: string;
    //predicciones: Prediccion[];
    puntosExactoEquipo: number;
    puntosGanadorOEmpate: number;
    puntosDiferencia: number;
    uniqueCode: string;
  }