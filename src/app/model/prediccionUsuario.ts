import { Prediccion } from "./prediccion";
import { Usuario } from "./usuario";

export class PrediccionUsuario {
    usuario: Usuario;
    prediccionPartido: Prediccion[];
  }