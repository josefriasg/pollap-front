import { Pais } from "./pais";

export class Equipo {
    idEquipo: number;
    name: string;
    pais:Pais;
    externalId: number;
    urlEscudo: string;
  }