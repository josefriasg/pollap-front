export class Torneo {
    idTorneo: number;
    name: string;
    externalId: number;
    active: boolean;
  }