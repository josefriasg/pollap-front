import { Partido } from "./partido";

export class Prediccion {
    idPrediccionPartido: number;
    partido: Partido;
    resultadoEquipo1: number;
    resultadoEquipo2: number;
  }