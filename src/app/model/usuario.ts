
export class Usuario {
    userId: number;
    name: string;
    email:string;
    role:string;
    picture:string;
  }