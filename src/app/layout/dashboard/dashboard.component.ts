import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { PollasService } from '../../services/pollas.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];

    pollas:number;
    @Input() codigoPolla:string;
    errorMessageJoin: string;
  successMessageJoin: string;

    constructor(private pollaService: PollasService) {
        this.sliders.push(
            {
                imagePath: 'assets/images/slider1.png',
                label: 'EJEMPLO',
                text:'Esta es una imagen de ejemplo'
            },
            {
                imagePath: 'assets/images/slider2.png',
                label: 'EJEMPLO',
                text: 'Esta es una imagen de ejemplo'//,
                //link:"/assets/manualConvivencia.pdf"
            }
        );
    }

    ngOnInit() {
        this.pollaService.getCountPollas().subscribe(count => this.pollas = count);
    }

    unirseAPolla():void{
        this.pollaService.joinPolla(this.codigoPolla).subscribe(result => this.processJoinResult(result));
      }

      processJoinResult(result:any){
        if (result==0){
          this.errorMessageJoin=null;
          this.successMessageJoin='Se ha unido a la polla';
        }else if (result==-1){
          this.successMessageJoin=null;
          this.errorMessageJoin='No existe una polla con ese código';
        }else if (result == -2){
          this.successMessageJoin=null;
          this.errorMessageJoin='No se pudo unir a la polla';
        }else if (result == -3){
          this.successMessageJoin=null;
          this.errorMessageJoin='Usted ya pertenece a esa polla';
        }
      }  


    openLink(link:string):void{
        if (link && link.length>0){
            window.open(link);
        }
        
    }

}
