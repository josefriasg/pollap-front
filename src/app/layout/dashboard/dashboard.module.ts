import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { FormsModule } from '@angular/forms';
//import {
//    TimelineComponent,
//    NotificationComponent,
//    ChatComponent
//} from './components';
import { StatModule } from '../../shared';
import { PollasService } from '../../services/pollas.service';

@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule.forRoot(),
        NgbAlertModule.forRoot(),
        DashboardRoutingModule,
        FormsModule,
        StatModule
    ],
    declarations: [
        DashboardComponent
        //TimelineComponent,
        //NotificationComponent,
        //ChatComponent
    ],providers: [
        PollasService
      ]
})
export class DashboardModule {}
