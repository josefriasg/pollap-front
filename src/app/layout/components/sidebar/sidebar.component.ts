import { Component, Output, EventEmitter, OnInit, Input } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CommentsService } from '../../../services/comments.service';
import { AlertsService } from '../../../services/alerts.service';
import { PictureService } from '../../../services/picture.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
    isActive: boolean = false;
    collapsed: boolean = false;
    showMenu: string = '';
    pushRightClass: string = 'push-right';
    username: string;
    role: string;
    isAdmin: boolean;
    isUser: boolean;
    picture:string ='';
    subscription: Subscription;
    @Output() collapsedEvent = new EventEmitter<boolean>();

    closeResult: string;
    @Input() comment : string ;
    
    constructor(private translate: TranslateService, public router: Router, private modalService: NgbModal, private commentService: CommentsService, private alertService: AlertsService, private pictureService: PictureService) {
        this.subscription = this.pictureService.picture$.subscribe(
            pictureData => {
                this.picture = 'data:image/jpeg;base64,' +pictureData;
            }
        );

        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de']);
        this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'en');
        if (sessionStorage.getItem('data')!=null){
            this.username=JSON.parse(sessionStorage.getItem('data')).name;
            this.role=JSON.parse(sessionStorage.getItem('data')).role;
            this.isAdmin=this.role=='1';
            this.isUser=this.role=='0';
        }

        if (localStorage.getItem('picture')!=null){
            this.picture = 'data:image/jpeg;base64,' +localStorage.getItem('picture');
        }else{
            this.picture = null;
        }

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    eventCalled() {
        this.isActive = !this.isActive;
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    toggleCollapsed() {
        this.collapsed = !this.collapsed;
        this.collapsedEvent.emit(this.collapsed);
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
    }

    newComment(content : any):void{
        this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
                this.closeResult = `Closed with: ${result}`;
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
      }

      private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    onSubmit(c:any){
        this.commentService.saveComentario(this.comment).subscribe(result => this.afterSave(c, result));
      }

      afterSave(c:any, result:any){
        
        if (result == 0){
            this.comment = "";
            c('Sent');
            this.alertService.alertSuccessMessage('Comentario enviado');
          }else if (result == -1){
            this.alertService.alertErrorMessage('Sólo se admiten 255 caracteres');
          }else{
            this.alertService.alertErrorMessage('No se pudo enviar el comentario');
          }
      }

      ngOnDestroy() {
        // prevent memory leak when component destroyed
        this.subscription.unsubscribe();
      }
}
