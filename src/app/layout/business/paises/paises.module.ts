import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaisesComponent } from './paises.component';
import { PaisesRoutingModule } from './paises-routing.module';
import { PageHeaderModule } from '../../../shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { PaisesService } from '../../../services/paises.service';

@NgModule({
  declarations: [PaisesComponent],
  imports: [
    CommonModule, PageHeaderModule, FormsModule, NgbModule.forRoot(),
    PaisesRoutingModule
  ],providers: [
    PaisesService
  ]
})
export class PaisesModule { }
