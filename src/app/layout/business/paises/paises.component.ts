import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { Pais } from '../../../model/pais';
import { PaisesService } from '../../../services/paises.service';
import { AlertsService } from '../../../services/alerts.service';

@Component({
  selector: 'app-paises',
  templateUrl: './paises.component.html',
  styleUrls: ['./paises.component.scss']
})
export class PaisesComponent implements OnInit {
  paises : Pais[];
  @Input() selectedPais : Pais ;
  closeResult: string;
  readOnly : boolean = false;
  constructor(private paisService: PaisesService, private modalService: NgbModal , private alertService: AlertsService) { }

  ngOnInit() {
    this.getPaises();
    this.readOnly=false;
  }

  onSelect(pais: Pais): void {
    this.selectedPais = pais;
  }

  getPaises(): void {
    //this.paises = this.paisService.getPaises();
    this.paisService.getPaises().subscribe(paises => this.paises = paises);
  }
  
  createNewPais(content : any): void {
    let newPais : Pais = {idPais:0, name:'', externalId:''};
    this.selectedPais = newPais;
    this.readOnly=false;
   this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
  }
  
  editPais(pais: Pais, content : any):void{
    this.paisService.getPais(pais.idPais).subscribe(pais=>this.selectedPais=pais);
    this.readOnly=false;
    this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
  }
  
  deletePais(pais: Pais):void{
    if (confirm('¿Está seguro que quiere eliminar el pais?')){
      this.paisService.deletePais(pais.idPais).subscribe(result => this.getPaises());
    }
  }
  
  viewPais(pais: Pais, content : any):void{
    this.paisService.getPais(pais.idPais).subscribe(pais=>this.selectedPais=pais);
    this.readOnly=true;
    this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
  }
  
  onSubmit(c:any){
    this.savePais(c);
  }
  savePais(c:any): void {
    this.paisService.savePais(this.selectedPais).subscribe(result => this.afterSave(c));
  }

  afterSave(c:any){
    this.getPaises();
    c('Saved');
    this.alertService.alertSuccessMessage('Guardado exitosamente');
  }

  private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }
}
