import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrearPollaComponent } from './crear-polla.component';
import { CrearPollaRoutingModule } from './crear-polla-routing.module';
import { FormsModule } from '@angular/forms';
import { PageHeaderModule } from '../../../shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { TorneosService } from '../../../services/torneos.service';
import { PollasService } from '../../../services/pollas.service';

@NgModule({
  declarations: [CrearPollaComponent],
  imports: [
    CommonModule,PageHeaderModule, FormsModule, NgbModule.forRoot(),
    CrearPollaRoutingModule
  ],providers: [
    TorneosService, PollasService
  ]
})
export class CrearPollaModule { }
