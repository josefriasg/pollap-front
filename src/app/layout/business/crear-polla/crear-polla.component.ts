import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Torneo } from '../../../model/torneo';
import { TorneosService } from '../../../services/torneos.service';
import { AlertsService } from '../../../services/alerts.service';
import { PollasService } from '../../../services/pollas.service';
import { Form, NgForm } from '@angular/forms';
import { Polla } from '../../../model/polla';

@Component({
  selector: 'app-crear-polla',
  templateUrl: './crear-polla.component.html',
  styleUrls: ['./crear-polla.component.scss']
})
export class CrearPollaComponent implements OnInit {
  selectedPolla : Polla = {idPolla:0, name:null, torneo:null, creador:null, emailsInvitados:null, puntosExactoEquipo:2, puntosGanadorOEmpate:2, puntosDiferencia:2, uniqueCode:null};
  torneos : Torneo[];
  errorMessageReserva: string;
  successMessageReserva: string;
  @ViewChild('form') myForm: NgForm;

  constructor(private pollaService: PollasService ,private torneoService: TorneosService, private alertService:AlertsService) { }

  ngOnInit() {
    this.getTorneos();
  }

  getTorneos():void{
    this.torneoService.getActiveTorneos().subscribe(torneos => this.torneos = torneos);
  }

  selectTorneo(torneo:Torneo):void{
    this.selectedPolla.torneo=torneo;
}

  compareTorneo(c1: Torneo, c2: Torneo): boolean {
    return c1 && c2 ? c1.idTorneo === c2.idTorneo : c1 === c2;
  }

  onSubmit(){
    this.savePolla();
  }

  savePolla(): void {
    console.log("Llamando guardar");
    this.pollaService.savePolla(this.selectedPolla).subscribe(result => this.processResult(result));
  }

  afterSave(){
    this.alertService.alertSuccessMessage('Guardado exitosamente');
    this.cleanData();
  }
  processResult (result:any){
    if (result>0){
      this.errorMessageReserva=null;
      this.afterSave()  
    }else if (result==-1){
      this.errorMessageReserva='Ya tiene una polla guardada con el mismo nombre';
    }else if (result == -2){
      this.errorMessageReserva='No se pudo guardar la polla';
    }
    
  }

  cleanData(){
    this.myForm.resetForm({});
    this.selectedPolla = {idPolla:0, name:null, torneo:null, creador:null, emailsInvitados:null, puntosExactoEquipo:2, puntosGanadorOEmpate:2, puntosDiferencia:2, uniqueCode:null};
    this.errorMessageReserva = null;
    this.successMessageReserva = null;
    
  }

}
