import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CrearPollaComponent } from './crear-polla.component';
const routes: Routes = [
    {
        path: '', component: CrearPollaComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class CrearPollaRoutingModule { }
