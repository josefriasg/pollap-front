import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearPollaComponent } from './crear-polla.component';

describe('CrearPollaComponent', () => {
  let component: CrearPollaComponent;
  let fixture: ComponentFixture<CrearPollaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearPollaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearPollaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
