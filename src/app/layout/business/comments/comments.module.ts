import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from '../../../shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { CommentsComponent } from './comments.component';
import { CommentsRoutingModule } from './comments-routing.module';
import { CommentsService } from '../../../services/comments.service';

@NgModule({
  declarations: [CommentsComponent],
  imports: [
    CommonModule,
    PageHeaderModule, FormsModule, NgbModule.forRoot(),
    CommentsRoutingModule
  ],
  providers: [
    CommentsService
  ]
})
export class CommentsModule { }
