import { Component, OnInit, Input } from '@angular/core';
import { Comentario } from '../../../model/comentario';
import { CommentsService } from '../../../services/comments.service';
import { format} from 'date-fns';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
  comments : Comentario[];
  page: number=1;
  maxSize:number=5;
  pageSize:number=10;
  constructor(private comentarioService: CommentsService) { }

  ngOnInit() {
    this.getComments();
  }

  getComments(): void {
    this.comentarioService.getComentarios().subscribe(comments => this.comments = comments);
  }

  setPage(page: number) {
    if (page < 1){// || page > this.pager.totalPages) {
        return;
    }
    if (isNaN(page)) {
        page = 1;
    }
    this.page = page;
  }

  formatDate(date:Date):string{
    return (format(date, 'YYYY-MM-DD h:mm:ss A'));
  }
}
