import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import { MarcadoresComponent } from './marcadores.component';
const routes: Routes = [
    {
        path: '', component: MarcadoresComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarcadoresRoutingModule { }
