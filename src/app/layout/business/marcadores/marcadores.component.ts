import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Torneo } from '../../../model/torneo';
import { NgForm } from '@angular/forms';
import { TorneosService } from '../../../services/torneos.service';
import { AlertsService } from '../../../services/alerts.service';
import { PartidosService } from '../../../services/partidos.service';
import { Partido } from '../../../model/partido';
import { ResultadoOperacion } from '../../../model/resultadoOperacion';
import { format} from 'date-fns';

@Component({
  selector: 'app-marcadores',
  templateUrl: './marcadores.component.html',
  styleUrls: ['./marcadores.component.scss']
})
export class MarcadoresComponent implements OnInit {
  @Input() selectedTorneo : Torneo ;
  torneos : Torneo[];
  partidos : Partido[]=[];
  page: number=1;
  maxSize:number=5;
  pageSize:number=10;
  constructor(private torneoService:TorneosService, private alertService: AlertsService, private partidoService: PartidosService) { }

  ngOnInit() {
    this.torneoService.getTorneos().subscribe(torneos => this.torneos = torneos);
  }

  selectTorneo(torneo:Torneo):void{
    this.selectedTorneo = torneo;
    this.partidoService.getPartidosFromTorneo(this.selectedTorneo.idTorneo).subscribe(partidos => this.updatePartidos(partidos));
}

updatePartidos(partidos:Partido[]):void{
  this.partidos = partidos
}

saveMarcadores():void{
  this.partidoService.saveMarcadores(this.partidos).subscribe(result => this.processResult(result));
}

formatDate(date:Date):string{
  return (format(date, 'YYYY-MM-DD h:mm:ss A'));
}

processResult(result:ResultadoOperacion){
  if (result.code==0){
    this.alertService.alertSuccessMessage(result.message);
  }else if (result.code!=0){
    this.alertService.alertErrorMessage(result.message);
  }
}

setPage(page: number) {
  if (page < 1){// || page > this.pager.totalPages) {
      return;
  }
  if (isNaN(page)) {
      page = 1;
  }
  this.page = page;
}

}
