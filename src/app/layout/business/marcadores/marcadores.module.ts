import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarcadoresComponent } from './marcadores.component';
import { MarcadoresRoutingModule } from './marcadores-routing.module';
import { PartidosService } from '../../../services/partidos.service';
import { TorneosService } from '../../../services/torneos.service';
import { PageHeaderModule } from '../../../shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [MarcadoresComponent],
  imports: [
    CommonModule, PageHeaderModule, FormsModule, NgbModule.forRoot(),
    MarcadoresRoutingModule
  ],providers: [
    PartidosService, TorneosService
  ]
})
export class MarcadoresModule { }
