import { Component, OnInit, Input } from '@angular/core';
import { LoginService } from '../../../services/login.service';
import { UsuarioService } from '../../../services/usuario.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { AlertsService } from '../../../services/alerts.service';
import { PictureService } from '../../../services/picture.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  nombre:string;
  email:string;
  @Input() password:string;
  @Input() newPassword:string;
  @Input() confirmPassword:string;
  closeResult: string;
  selectedFile: File;
  notification: boolean;
  constructor(private loginService:LoginService, private usuarioService:UsuarioService, private pictureService:PictureService, private modalService: NgbModal, private alertService: AlertsService) { }

  ngOnInit() {
      this.nombre=JSON.parse(sessionStorage.getItem('data')).name;
      this.email=JSON.parse(sessionStorage.getItem('data')).username;
      this.usuarioService.loadNotification().subscribe(result => this.notification = result);
  }

  changePassword(content : any): void {
    this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.password= null;
      this.newPassword= null;
      this.confirmPassword=null;
  }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.password = null;
      this.newPassword= null;
      this.confirmPassword=null;
  });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
    } else {
        return  `with: ${reason}`;
    }
}

onSubmit(c:any){
  this.loginService.changePassword(this.password, this.newPassword).subscribe(result => this.afterSave(c, result));
}


afterSave(c:any, result:boolean){
  c('Saved');
  if (result){
    this.alertService.alertSuccessMessage('Contraseña cambiada exitosamente');
  }else{
    this.alertService.alertErrorMessage('La contraseña no se pudo cambiar');
  }
  
}

public changeProfilePicture(content : any):void{
  this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
    this.selectedFile = null;
}, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    this.selectedFile = null;
});
}

public selectFile(event:any):void {
  this.selectedFile = event.target.files[0];
}
public uploadPicture(c:any):void {
      const uploadImageData = new FormData();
      uploadImageData.append('imageFile', this.selectedFile, this.selectedFile.name);
      this.usuarioService.uploadImage(uploadImageData).subscribe(result => this.afterImageUpdate(c, result));
    }

  public afterImageUpdate(c:any, result:any):void{
    //TODO actualizar usuario en sesión y eso actualiza imagen en header de una?
    //Validar mensajes de error (por tipo de archivo, tamaño etc.)
    
    if (result==0){
      this.alertService.alertSuccessMessage('Foto de perfil actualizada');
      this.usuarioService.getCurUserWithProfilePicture().subscribe(user =>{
        this.pictureService.updatePicture(user.picture);
    })
      c('Uploaded');
    }else if (result==1){
      this.alertService.alertErrorMessage('El tamaño del archivo excede lo permitido');
    }else if (result==2){
      this.alertService.alertErrorMessage('Error con el formato del archivo');
    }else{
      this.alertService.alertErrorMessage('No se pudo actualizar la foto');
    }
  }

  public updateNotification(){
    this.usuarioService.updateNotification(!this.notification).subscribe(result => {
      if (result){
        this.alertService.alertSuccessMessage('Preferencias actualizadas');
        this.notification = !this.notification;
      }else{
        this.alertService.alertErrorMessage('Error actualizando preferencias');
      }
    });
  }

}
