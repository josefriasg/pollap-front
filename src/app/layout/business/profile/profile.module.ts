import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from '../../../shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { ProfileComponent } from './profile.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { LoginService } from '../../../services/login.service';
import { PasswordConfirmationModule } from '../../../shared/modules/password-confirmation/password-confirmation.module';
import { UsuarioService } from '../../../services/usuario.service';

@NgModule({
  declarations: [ProfileComponent],
  imports: [
    CommonModule, PasswordConfirmationModule,
    PageHeaderModule, FormsModule, NgbModule.forRoot(),
    ProfileRoutingModule
  ],
  providers: [
    LoginService, UsuarioService
  ]
})
export class ProfileModule { }
