import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EquiposComponent } from './equipos.component';
import { EquiposRoutingModule } from './equipos-routing.module';
import { PageHeaderModule } from '../../../shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { EquiposService } from '../../../services/equipos.service';
import { PaisesService } from '../../../services/paises.service';

@NgModule({
  declarations: [EquiposComponent],
  imports: [
    CommonModule, PageHeaderModule, FormsModule, NgbModule.forRoot(),
    EquiposRoutingModule
  ],providers: [
    EquiposService, PaisesService
  ]
})
export class EquiposModule { }
