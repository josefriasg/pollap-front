import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { Equipo } from '../../../model/equipo';
import { Pais } from '../../../model/pais';
import { EquiposService } from '../../../services/equipos.service';
import { PaisesService } from '../../../services/paises.service';
import { AlertsService } from '../../../services/alerts.service';

@Component({
  selector: 'app-equipos',
  templateUrl: './equipos.component.html',
  styleUrls: ['./equipos.component.scss']
})
export class EquiposComponent implements OnInit {
  equipos : Equipo[];
  @Input() selectedEquipo : Equipo ;
  @Input() selectedPais : Pais ;
  @Input() newEquipoName: string;
  @Input() newIdExterno: string;
  @Input() newUrlEscudo: string;
  paises : Pais[];
  closeResult: string;
  readOnly : boolean = false;
  constructor(private paisService:PaisesService, private equipoService: EquiposService, private modalService: NgbModal , private alertService: AlertsService) { }

  ngOnInit() {
    this.getPaises();
    this.readOnly=false;
  }

  getPaises(): void {
    this.paisService.getPaises().subscribe(paises => this.paises = paises);
  }

  selectPais(pais:Pais):void{
    this.selectedPais = pais;
    this.equipos = [];
    this.equipoService.getEquiposFromPais(this.selectedPais.idPais).subscribe(equipos => this.equipos = equipos);
}

  onSelect(equipo: Equipo): void {
    this.selectedEquipo = equipo;
  }

  getEquipos(): void {
    //this.equipos = this.equipoService.getEquipos();
    if (this.selectedPais){
      this.equipoService.getEquiposFromPais(this.selectedPais.idPais).subscribe(equipos => this.equipos = equipos);
    }
    
  }
  
  createNewEquipo(content : any): void {
    if (this.newEquipoName != null){
      let newEquipo : Equipo = {idEquipo:0, name:this.newEquipoName, pais:this.selectedPais, externalId: +this.newIdExterno, urlEscudo:this.newUrlEscudo};
      this.selectedEquipo = newEquipo;
      this.saveEquipo(null);
      this.selectedEquipo = null;
      this.newEquipoName = null;
    }
    
  }
  
  editEquipo(equipo: Equipo, content : any):void{
    this.equipoService.getEquipo(equipo.idEquipo).subscribe(equipo=>this.selectedEquipo=equipo);
    this.readOnly=false;
    this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
  }
  
  deleteEquipo(equipo: Equipo):void{
    if (confirm('¿Está seguro que quiere eliminar el equipo?')){
      this.equipoService.deleteEquipo(equipo.idEquipo).subscribe(result => this.getEquipos());
    }
  }
  
  viewEquipo(equipo: Equipo, content : any):void{
    this.equipoService.getEquipo(equipo.idEquipo).subscribe(equipo=>this.selectedEquipo=equipo);
    this.readOnly=true;
    this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
  }
  
  onSubmit(c:any){
    this.saveEquipo(c);
  }
  saveEquipo(c:any): void {
    this.equipoService.saveEquipo(this.selectedEquipo).subscribe(result => this.afterSave(c));
  }

  afterSave(c:any){
    this.getEquipos();
    if (c!=null){
      c('Saved');
    }
    this.alertService.alertSuccessMessage('Guardado exitosamente');
  }

  private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }
}
