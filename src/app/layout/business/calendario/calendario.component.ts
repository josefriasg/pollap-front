import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef, OnInit, Input
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
  format,
  parse
} from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView
} from 'angular-calendar';

import { EventoCalendario } from '../../../model/eventoCalendario';
import { PollasService } from '../../../services/pollas.service';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'app-calendario',
  //changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './calendario.component.html',
  styleUrls: ['./calendario.component.scss']
})
export class CalendarioComponent implements OnInit {
  
  @ViewChild('modalContent', { read: true }) modalContent: TemplateRef<any>;
//@ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [];
  /*[
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];*/

  refresh: Subject<any> = new Subject();
  locale: string = "es";

  events: CalendarEvent[]= [];

  activeDayIsOpen: boolean = false;
  currentMonth:number;
  constructor(private pollaService: PollasService,private modal: NgbModal) { }

  ngOnInit() {
    this.currentMonth = this.viewDate.getMonth();
    this.getPredicciones();
  }

  getPredicciones(): void {
    this.events = [];
    this.pollaService.getPrediccionesPollas(this.viewDate).subscribe(eventos => this.addEventos(eventos));
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }

  /*eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }*/

  addEvent(startParam:Date, endParam:Date, titleParam:string, colorParam:any): void {
    this.events = [
      ...this.events,
      {
        title: titleParam,//'New event',
        start: startParam,//startOfDay(new Date()),
        end: endParam,//endOfDay(new Date()),
        color: colorParam,//colors.red,
        /*draggable: false,
        resizable: {
          beforeStart: false,
          afterEnd: false
        }*/
      }
    ];
  }

  /*deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter(event => event !== eventToDelete);
  }*/

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
    if(this.viewDate.getMonth()!=this.currentMonth){
      this.currentMonth=this.viewDate.getMonth();
      this.getPredicciones();
    }
  }


  addEventos(eventos:EventoCalendario[]):void{
    for (var evento of eventos){
      let startDateObj = parse(evento.start.valueOf());
    let endDateObj = parse(evento.end.valueOf());
      let startFormat = this.formatTime(startDateObj);
      let endFormat = this.formatTime(endDateObj);
      
      let color:any;
      if (evento.color=='red'){
        color=colors.red;
      }else{
        color=colors.blue;
      }
      this.addEvent(startDateObj, endDateObj, evento.text,color);
    }
    this.refresh.next();
  }

  formatTime(date:Date):string{
    return (format(date, 'h:mm:ss A'));
    /*let formatted:string;
    let dat = new Date(date);

    let extra0 = '';
    if (dat.getMinutes()<10){
      extra0 = '0';
    }

    let meridian = 'AM';
    let hours = dat.getHours();
    if (dat.getHours()>12){
      hours = hours - 12;
    }

    if (dat.getHours()>11){
      meridian = 'PM';
    }
    
    formatted = hours+':'+extra0+dat.getMinutes()+' '+meridian;
    return formatted;*/
    //return new Date(date).toLocaleString();
  }
}
