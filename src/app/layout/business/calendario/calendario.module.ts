import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarioComponent } from './calendario.component';
import { CalendarioRoutingModule } from './calendario-routing.module';
import { PageHeaderModule } from '../../../shared';
import { NgbModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { PollasService } from '../../../services/pollas.service';

registerLocaleData(localeEs);


@NgModule({
  declarations: [CalendarioComponent],
  imports: [
    CommonModule,PageHeaderModule, FormsModule, NgbModule.forRoot(),
    NgbModalModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    CalendarioRoutingModule
  ],providers: [
    PollasService
  ],exports: [CalendarioComponent]
})
export class CalendarioModule { }
