import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { Deporte } from '../../../model/deporte';
import { DeportesService } from '../../../services/deportes.service';
import { AlertsService } from '../../../services/alerts.service';

@Component({
  selector: 'app-deportes',
  templateUrl: './deportes.component.html',
  styleUrls: ['./deportes.component.scss']
})
export class DeportesComponent implements OnInit {
  deportes : Deporte[];
  @Input() selectedDeporte : Deporte ;
  closeResult: string;
  readOnly : boolean = false;
  constructor(private deporteService: DeportesService, private modalService: NgbModal , private alertService: AlertsService) { }

  ngOnInit() {
    this.getDeportes();
    this.readOnly=false;
  }

  onSelect(deporte: Deporte): void {
    this.selectedDeporte = deporte;
  }

  getDeportes(): void {
    //this.deportes = this.deporteService.getDeportes();
    this.deporteService.getDeportes().subscribe(deportes => this.deportes = deportes);
  }
  
  createNewDeporte(content : any): void {
    let newDeporte : Deporte = {idDeporte:0, name:''};
    this.selectedDeporte = newDeporte;
    this.readOnly=false;
   this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
  }
  
  editDeporte(deporte: Deporte, content : any):void{
    this.deporteService.getDeporte(deporte.idDeporte).subscribe(deporte=>this.selectedDeporte=deporte);
    this.readOnly=false;
    this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
  }
  
  deleteDeporte(deporte: Deporte):void{
    if (confirm('¿Está seguro que quiere eliminar el deporte?')){
      this.deporteService.deleteDeporte(deporte.idDeporte).subscribe(result => this.getDeportes());
    }
  }
  
  viewDeporte(deporte: Deporte, content : any):void{
    this.deporteService.getDeporte(deporte.idDeporte).subscribe(deporte=>this.selectedDeporte=deporte);
    this.readOnly=true;
    this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
  }
  
  onSubmit(c:any){
    this.saveDeporte(c);
  }
  saveDeporte(c:any): void {
    this.deporteService.saveDeporte(this.selectedDeporte).subscribe(result => this.afterSave(c));
  }

  afterSave(c:any){
    this.getDeportes();
    c('Saved');
    this.alertService.alertSuccessMessage('Guardado exitosamente');
  }

  private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }
}
