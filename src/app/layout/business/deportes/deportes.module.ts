import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeportesComponent } from './deportes.component';
import { DeportesRoutingModule } from './deportes-routing.module';
import { PageHeaderModule } from '../../../shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { DeportesService } from '../../../services/deportes.service';

@NgModule({
  declarations: [DeportesComponent],
  imports: [
    CommonModule, PageHeaderModule, FormsModule, NgbModule.forRoot(),
    DeportesRoutingModule
  ],providers: [
    DeportesService
  ]
})
export class DeportesModule { }
