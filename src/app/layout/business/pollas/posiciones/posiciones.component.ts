import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Observable, interval, Subscription, of } from 'rxjs';
import { Posicion } from '../../../../model/posicion';
import { Polla } from '../../../../model/polla';
import { PollasService } from '../../../../services/pollas.service';
import { AlertsService } from '../../../../services/alerts.service';
import { ActivatedRoute } from '@angular/router';
import { Prediccion } from '../../../../model/prediccion';
import { format } from 'date-fns';
import * as esLocale from 'date-fns/locale/es/index.js'

@Component({
  selector: 'app-posiciones',
  templateUrl: './posiciones.component.html',
  styleUrls: ['./posiciones.component.scss']
})
export class PosicionesComponent implements OnInit, OnDestroy {
  private static refreshSeconds:number=10;
  posiciones : Posicion[] = [];
  posicionesWithPictureCache : Posicion[] = [];
  currentPosicion: Posicion;
  closeResult: string;
  idPolla:number;
  polla : Polla;
  predicciones: Prediccion[] = [];
  posicionesPage: number=1;
  prediccionesPage: number=1;
  maxSize:number=5;
  pageSize:number=50;
  private sub: any;
  private updateSubscription: Subscription;
  private resultsSubscription: Subscription;
  constructor(private pollaService: PollasService, private modalService: NgbModal , private alertService: AlertsService,  private route: ActivatedRoute) { }
  
  

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.idPolla = +params['id'];
      this.pollaService.getPollaBasic(this.idPolla).subscribe(polla => this.polla = polla);
      this.getPosicionesFromServerWithPicture(this.idPolla);
   });

   this.updateSubscription = interval(PosicionesComponent.refreshSeconds*1000).subscribe(
    (val) => { this.getPosicionesFromServer(this.idPolla);
    }
    );

    this.resultsSubscription = interval(PosicionesComponent.refreshSeconds*1000).subscribe(
      (val) => { this.checkResultadosPosicion();
      }
      );
  }

  ngOnDestroy(): void {
    this.updateSubscription.unsubscribe();
    this.resultsSubscription.unsubscribe();
  }

  checkResultadosPosicion():void{
    if (this.currentPosicion && this.currentPosicion != null){
      this.getPrediccionPollaFromUser(this.currentPosicion.userId);
    }
  }

  get refreshSeconds() {
    return PosicionesComponent.refreshSeconds;
  }

  getPosicionesFromServer(idPolla:number):void{
    this.pollaService.getPosiciones(this.idPolla).subscribe(posiciones => this.updatePosiciones(posiciones))
  }

  getPosicionesFromServerWithPicture(idPolla:number):void{
    this.pollaService.getPosicionesWithPicture(this.idPolla).subscribe(posiciones => this.updatePosicionesWithPicture(posiciones))
  }

  updatePosiciones(posiciones:Posicion[]):void{
    for (let posicion of posiciones){
      for (let posicionPicture of this.posicionesWithPictureCache){
        if (posicion.userId == posicionPicture.userId){
          posicion.picture = posicionPicture.picture;
          break;
        }
      }
    }
    this.posiciones = posiciones;
  }

  updatePosicionesWithPicture(posiciones:Posicion[]):void{
    this.posicionesWithPictureCache = posiciones;
    this.posiciones = posiciones;
  }
  
  viewResultados(posicion: Posicion, content : any):void{
    this.getPrediccionPollaFromUser(posicion.userId);
    this.currentPosicion = posicion;
    this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
      this.prediccionesPage=1;
      this.predicciones = [];
      this.currentPosicion=null;
      this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.prediccionesPage=1;
    this.predicciones = [];
    this.currentPosicion=null;
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
  }

  getPrediccionPollaFromUser(idUser : number):void{
    this.pollaService.getPrediccionPollaFromUser(this.idPolla, idUser).subscribe(predicciones => this.getPredicciones(predicciones));
  }

  getPredicciones(predicciones:Prediccion[]):void{
    this.predicciones = predicciones;
  }

  mustPrintDate(index:number, indexPage:number):boolean{
    if (index==0||indexPage==0){
      return true;
    }else{
      let prevPred = this.predicciones[index-1];
      let pred = this.predicciones[index];
      return (this.formatDate(prevPred.partido.inicio)!=this.formatDate(pred.partido.inicio));
    }
  }

  formatDate(date:Date):string{
    let dateString = (format(date, 'dddd [-] MMMM D, YYYY', {locale: esLocale}));
    return dateString.substr(0,1).toUpperCase()+dateString.substr(1);
  }

  formatTime(date:Date):string{
    return  (format(date, 'h:mm:ss A', {locale: esLocale}));
  }

  private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    setPrediccionesPage(prediccionesPage: number) {
      if (prediccionesPage < 1){// || page > this.pager.totalPages) {
          return;
      }
      if (isNaN(prediccionesPage)) {
        prediccionesPage = 1;
      }
  
      this.prediccionesPage = prediccionesPage;
  }

  setPosicionesPage(posicionesPage: number) {
    if (posicionesPage < 1){// || page > this.pager.totalPages) {
        return;
    }
    if (isNaN(posicionesPage)) {
      posicionesPage = 1;
    }
    this.posicionesPage = posicionesPage;
}

calcularPuntos(prediccion:Prediccion):string{
  let puntos:number=0;

  let difMarcadorPred:number = prediccion.resultadoEquipo1 - prediccion.resultadoEquipo2;
  let difMarcadorReal:number = prediccion.partido.resultadoFinalEquipo1 - prediccion.partido.resultadoFinalEquipo2;

  if (this.polla.puntosDiferencia>0 && difMarcadorPred == difMarcadorReal){
    puntos+=this.polla.puntosDiferencia;
  }

  if (this.polla.puntosExactoEquipo>0){
    if (prediccion.resultadoEquipo1 == prediccion.partido.resultadoFinalEquipo1){
      puntos+=this.polla.puntosExactoEquipo;
    }

    if (prediccion.resultadoEquipo2 == prediccion.partido.resultadoFinalEquipo2){
      puntos+=this.polla.puntosExactoEquipo;
    }
  }

  if (this.polla.puntosGanadorOEmpate>0){
    if (difMarcadorPred>0 && difMarcadorReal>0){
      puntos+=this.polla.puntosGanadorOEmpate;
    }else if (difMarcadorPred<0 && difMarcadorReal<0){
      puntos+=this.polla.puntosGanadorOEmpate;
    }else if (difMarcadorPred==0 && difMarcadorReal==0){
      puntos+=this.polla.puntosGanadorOEmpate;
    }
  }
  
  return ""+puntos;
}

viewReglas(content:any):void{
    this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
}
