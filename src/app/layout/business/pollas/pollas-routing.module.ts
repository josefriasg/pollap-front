import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import { PollasComponent } from './pollas.component';
import { ListaPollaComponent } from './lista-polla/lista-polla.component';
import { PosicionesComponent } from './posiciones/posiciones.component';
import { ResultadosComponent } from './resultados/resultados.component';
import { VerResultadosComponent } from './ver-resultados/ver-resultados.component';
const routes: Routes = [
    {
        path: '', component: PollasComponent,
        children: [
          {path: '', redirectTo: 'lista' },
          {path: 'lista',  component: ListaPollaComponent},
          {path: 'posiciones/:id', component: PosicionesComponent},
          {path: 'resultados/:action/:id', component: ResultadosComponent},
          {path: 'ver-resultados/:id', component: VerResultadosComponent}
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PollasRoutingModule { }
