import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PollasComponent } from './pollas.component';
import { PollasRoutingModule } from './pollas-routing.module';
import { PageHeaderModule } from '../../../shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { PollasService } from '../../../services/pollas.service';
import { ListaPollaComponent } from './lista-polla/lista-polla.component';
import { PosicionesComponent } from './posiciones/posiciones.component';
import { ResultadosComponent } from './resultados/resultados.component';
// import { MatTableModule, MatButtonModule, MatIconModule, MatPaginatorModule, MatSortModule, MatPaginatorIntl, MatTooltipModule} from '@angular/material';
import { PaginatorEspanol } from '../../../model/paginator-espanol';
import { VerResultadosComponent } from './ver-resultados/ver-resultados.component';


@NgModule({
  declarations: [PollasComponent, ListaPollaComponent, PosicionesComponent, ResultadosComponent, VerResultadosComponent],
  imports: [
    CommonModule, PageHeaderModule, FormsModule, NgbModule.forRoot(),
    PollasRoutingModule
    // , MatTableModule, MatIconModule,MatButtonModule, MatPaginatorModule, MatSortModule, MatTooltipModule
  ],providers: [
    PollasService
    // { provide: MatPaginatorIntl, useClass: PaginatorEspanol}
  ]
})
export class PollasModule { }
