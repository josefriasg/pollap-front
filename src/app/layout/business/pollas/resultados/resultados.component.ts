import { Component, OnInit, Input } from '@angular/core';
import { PollasService } from '../../../../services/pollas.service';
import { Prediccion } from '../../../../model/prediccion';
import { AlertsService } from '../../../../services/alerts.service';
import { ResultadoOperacion } from '../../../../model/resultadoOperacion';
import { ActivatedRoute } from '@angular/router';
import { format, isFuture, parse } from 'date-fns';
import { Polla } from '../../../../model/polla';
import * as esLocale from 'date-fns/locale/es/index.js'

@Component({
  selector: 'app-resultados',
  templateUrl: './resultados.component.html',
  styleUrls: ['./resultados.component.scss']
})
export class ResultadosComponent implements OnInit {
  predicciones: Prediccion[]=[];
  idPolla:number;
  polla : Polla;
  private sub: any;
  page: number=1;
  maxSize:number=5;
  pageSize:number=10;
  action:string;
  constructor(private pollaService: PollasService, private alertService: AlertsService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.action = params['action'];
      this.idPolla = +params['id'];
      this.pollaService.getPollaBasic(this.idPolla).subscribe(polla => this.polla = polla);
      this.pollaService.getPrediccionPolla(this.idPolla, this.action).subscribe(predicciones => this.getPredicciones(predicciones));
   });
  }

  getPredicciones(predicciones:Prediccion[]):void{
    this.predicciones = predicciones;
  }

  savePredicciones():void{
    this.pollaService.savePrediccionPolla(this.predicciones, this.idPolla).subscribe(result => this.processResult(result));
  }

  mustPrintDate(index:number, indexPage:number):boolean{
    if (index==0||indexPage==0){
      return true;
    }else{
      let prevPred = this.predicciones[index-1];
      let pred = this.predicciones[index];
      return (this.formatDate(prevPred.partido.inicio)!=this.formatDate(pred.partido.inicio));
    }
  }

  formatDate(date:Date):string{
    let dateString = (format(date, 'dddd [-] MMMM D, YYYY', {locale: esLocale}));
    return dateString.substr(0,1).toUpperCase()+dateString.substr(1);
  }

  formatTime(date:Date):string{
    return  (format(date, 'h:mm:ss A', {locale: esLocale}));
  }

  isReadonly(prediccion:Prediccion):boolean{
    return prediccion.partido.resultadoFinalEquipo1>=0 ||  prediccion.partido.resultadoFinalEquipo2>=0
  }

  processResult(result:ResultadoOperacion){
    if (result.code==0){
      this.alertService.alertSuccessMessage(result.message);
    }else if (result.code!=0){
      this.alertService.alertErrorMessage(result.message);
    }
  }

  setPage(page: number) {
    if (page < 1){// || page > this.pager.totalPages) {
        return;
    }
    if (isNaN(page)) {
        page = 1;
    }
    this.page = page;
}

}
