import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaPollaComponent } from './lista-polla.component';

describe('ListaPollaComponent', () => {
  let component: ListaPollaComponent;
  let fixture: ComponentFixture<ListaPollaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaPollaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaPollaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
