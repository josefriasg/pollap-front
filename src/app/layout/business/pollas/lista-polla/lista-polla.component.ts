import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { Polla } from '../../../../model/polla';
import { PollasService } from '../../../../services/pollas.service';
import { AlertsService } from '../../../../services/alerts.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
// import {MatPaginator, MatTableDataSource, MatTable, MatSort} from '@angular/material';



@Component({
  selector: 'app-lista-polla',
  templateUrl: './lista-polla.component.html',
  styleUrls: ['./lista-polla.component.scss']
})
export class ListaPollaComponent implements OnInit {
  pollas : Polla[]=[];
  @Input() selectedPolla : Polla ;
  closeResult: string;
  readOnly : boolean = false;
  @Input() codigoPolla:string;
  errorMessageJoin: string;
  successMessageJoin: string;
  selectedPollaId:number;
  page: number=1;
  maxSize:number=5;
  pageSize:number=10;

  // displayedColumns: string[] = ['actions','name', 'code'];
  // dataSource :any;
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // @ViewChild(MatTable) table: MatTable<any>;

  constructor(private pollaService: PollasService, private modalService: NgbModal , private alertService: AlertsService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    // this.dataSource = new MatTableDataSource();
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
    this.getPollas();
    this.readOnly=false;
  }

  onSelect(polla: Polla): void {
    this.selectedPolla = polla;
  }

  getPollas(): void {
    //this.pollas = this.pollaService.getPollas();
    this.pollaService.getPollas().subscribe(pollas => this.updatePollas(pollas));
  }

  updatePollas(pollas:Polla[]){
    this.pollas = pollas;
    // this.dataSource.data = this.pollas; 
  }
  editable(polla:Polla):boolean{
    let username = JSON.parse(sessionStorage.getItem('data')).username;
    return polla.creador.email == username;
  }
  
  editPolla(polla: Polla, content : any):void{
    this.pollaService.getPolla(polla.idPolla).subscribe(polla=>this.selectedPolla=polla);
    this.readOnly=false;
    this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
  }

  goToPosiciones(polla:Polla):void{
    this.router.navigate(['/pollas/posiciones', polla.idPolla]);//, polla.idPolla, {idPolla:polla.idPolla} ]);
  }

  goToPrevResultados(polla:Polla):void{
    this.router.navigate(['/pollas/resultados/previous', polla.idPolla]);
  }

  goToAddResultados(polla:Polla):void{
    this.router.navigate(['/pollas/resultados/add', polla.idPolla]);
  }

  goToViewResultados(polla:Polla):void{
    this.router.navigate(['/pollas/ver-resultados', polla.idPolla]);
  }
  
  deletePolla(polla: Polla):void{
    if (confirm('¿Está seguro que quiere eliminar el polla?')){
      this.pollaService.deletePolla(polla.idPolla).subscribe(result => this.getPollas());
    }
  }
  
  onSubmit(c:any){
    this.savePolla(c);
  }
  savePolla(c:any): void {
    this.pollaService.savePolla(this.selectedPolla).subscribe(result => this.afterSave(c));
  }

  afterSave(c:any){
    this.getPollas();
    c('Saved');
    this.alertService.alertSuccessMessage('Guardado exitosamente');
  }

  unirseAPolla():void{
    this.pollaService.joinPolla(this.codigoPolla).subscribe(result => this.processJoinResult(result));
  }

  private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    processJoinResult(result:any){
      if (result==0){
        this.errorMessageJoin=null;
        this.successMessageJoin='Se ha unido a la polla';
        this.getPollas();
      }else if (result==-1){
        this.successMessageJoin=null;
        this.errorMessageJoin='No existe una polla con ese código';
      }else if (result == -2){
        this.successMessageJoin=null;
        this.errorMessageJoin='No se pudo unir a la polla';
      }else if (result == -3){
        this.successMessageJoin=null;
        this.errorMessageJoin='Usted ya pertenece a esa polla';
      }
    }  

    setPage(page: number) {
      if (page < 1){// || page > this.pager.totalPages) {
          return;
      }
      if (isNaN(page)) {
          page = 1;
      }
      this.page = page;
    }
}
