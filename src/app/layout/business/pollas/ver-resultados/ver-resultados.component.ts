import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { PollasService } from '../../../../services/pollas.service';
import { AlertsService } from '../../../../services/alerts.service';
import { PartidosService } from '../../../../services/partidos.service';
import { ActivatedRoute } from '@angular/router';
import { Polla } from '../../../../model/polla';
import { Partido } from '../../../../model/partido';
import { format } from 'date-fns';
import * as esLocale from 'date-fns/locale/es/index.js'
import { PrediccionUsuario } from '../../../../model/prediccionUsuario';
import { Observable, interval, Subscription, of } from 'rxjs';

@Component({
  selector: 'app-ver-resultados',
  templateUrl: './ver-resultados.component.html',
  styleUrls: ['./ver-resultados.component.scss']
})
export class VerResultadosComponent implements OnInit, OnDestroy {

  private static refreshSeconds:number=10;
  closeResult: string;
  idPolla:number;
  partidos: Partido[] = [];
  selectedPartido:Partido;
  polla : Polla;
  predicciones: PrediccionUsuario[] = [];
  partidosPage: number=1;
  maxSize:number=5;
  pageSize:number=10;
  private sub: any;
  private updateSubscription: Subscription;

  constructor(private pollaService: PollasService, private modalService: NgbModal , private alertService: AlertsService,  private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.idPolla = +params['id'];
      this.pollaService.getPollaBasic(this.idPolla).subscribe(polla => this.polla = polla);
      this.getPartidosEmpezadosFromServer(this.idPolla);
   });

   this.updateSubscription = interval(VerResultadosComponent.refreshSeconds*1000).subscribe(
    (val) => { this.getPartidosEmpezadosFromServer(this.idPolla);
    }
    );
  }

  ngOnDestroy(): void {
    this.updateSubscription.unsubscribe();
  }

  get refreshSeconds() {
    return VerResultadosComponent.refreshSeconds;
  }

  getPartidosEmpezadosFromServer(idPolla:number):void{
    this.pollaService.getPartidosEmpezadosFromPolla(this.idPolla).subscribe(partidos => this.updatePartidos(partidos));
  }

  updatePartidos(partidos:Partido[]){
    this.partidos = partidos;
  }

  mustPrintDate(index:number, indexPage:number):boolean{
    if (index==0||indexPage==0){
      return true;
    }else{
      let prev = this.partidos[index-1];
      let partido = this.partidos[index];
      return (this.formatDate(prev.inicio)!=this.formatDate(partido.inicio));
    }
  }

  formatDate(date:Date):string{
    let dateString = (format(date, 'dddd [-] MMMM D, YYYY', {locale: esLocale}));
    return dateString.substr(0,1).toUpperCase()+dateString.substr(1);
  }

  formatTime(date:Date):string{
    return  (format(date, 'h:mm:ss A', {locale: esLocale}));
  }

  setPartidosPage(page: number) {
    if (page < 1){// || page > this.pager.totalPages) {
        return;
    }
    if (isNaN(page)) {
        page = 1;
    }
    this.partidosPage = page;
}

verResultadosPartido(partido:Partido, content : any):void{
  this.selectedPartido=partido;
  this.pollaService.getPrediccionesPartidoPolla(partido.idPartido, this.idPolla).subscribe(predicciones => this.predicciones=predicciones);
  this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
    this.predicciones = [];
}, (reason) => {
  this.predicciones = [];
});
}

}
