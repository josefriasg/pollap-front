import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollasComponent } from './pollas.component';

describe('PollasComponent', () => {
  let component: PollasComponent;
  let fixture: ComponentFixture<PollasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
