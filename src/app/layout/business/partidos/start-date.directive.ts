import { Directive } from '@angular/core';
import { AbstractControl, FormGroup, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
export const dateValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const startDate = control.get('dps');
  const startTime = control.get('startTime');

  let now = new Date();

  if (startDate && startDate!=null && startDate.value != null && startDate.value.year != 0){
    let startDateObj = startDate.value;
    let startTimeObj = {hour: 0, minute: 0};
    if (startTime && startTime != null){
      startTimeObj = startTime.value;
    }
    
    if (startDateObj.year < now.getFullYear()){
      return { 'startDateBeforeToday': true };
    } else if (startDateObj.year == now.getFullYear()){
      
      if (startDateObj.month < now.getMonth()+1){
        return { 'startDateBeforeToday': true };
      }else if (startDateObj.month == now.getMonth()+1){
        if (startDateObj.day < now.getDate()){
          return { 'startDateBeforeToday': true };
        }else if (startDateObj.day == now.getDate()){
          if (startTimeObj.hour < now.getHours()){
            return { 'startDateBeforeToday': true };
          }else if (startTimeObj.hour == now.getHours()){
            if (startTimeObj.minute < now.getMinutes()){
              return { 'startDateBeforeToday': true };
            }
          }
        }
      }
    }
  }
  
  return null;
};
@Directive({
  selector: '[appStartDate]',
  providers: [{ provide: NG_VALIDATORS, useExisting: StartDateDirective, multi: true }]
})
export class StartDateDirective implements Validator{

  constructor() { }

  validate(control: AbstractControl): ValidationErrors {
    return dateValidator(control)
  }
}
