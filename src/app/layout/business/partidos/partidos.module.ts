import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PartidosComponent } from './partidos.component';
import { PartidosRoutingModule } from './partidos-routing.module';
import { PageHeaderModule } from '../../../shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { PartidosService } from '../../../services/partidos.service';
import { TorneosService } from '../../../services/torneos.service';
import { EquiposService } from '../../../services/equipos.service';
import { StartDateDirective } from './start-date.directive';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [PartidosComponent, StartDateDirective],
  imports: [
    CommonModule, NgSelectModule, PageHeaderModule, FormsModule, NgbModule.forRoot(),
    PartidosRoutingModule
  ],providers: [
    PartidosService, TorneosService, EquiposService
  ]
})
export class PartidosModule { }
