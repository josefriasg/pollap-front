import { Component, OnInit, Input, ViewChild, } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { Partido } from '../../../model/partido';
import { Equipo } from '../../../model/equipo';
import { Torneo } from '../../../model/torneo';
import { PartidosService } from '../../../services/partidos.service';
import { TorneosService } from '../../../services/torneos.service';
import { EquiposService } from '../../../services/equipos.service';
import { AlertsService } from '../../../services/alerts.service';
import { Form, NgForm } from '@angular/forms';
import { format, isFuture, parse } from 'date-fns';

@Component({
  selector: 'app-partidos',
  templateUrl: './partidos.component.html',
  styleUrls: ['./partidos.component.scss']
})
export class PartidosComponent implements OnInit {
  partidos : Partido[]=[];
  @Input() selectedPartido : Partido ;
  @Input() selectedTorneo : Torneo ;
  @Input() selectedEquipo1: Equipo;
  @Input() selectedEquipo2: Equipo;
  @Input() newIdExterno: string;
  startTime = {hour: 0, minute: 0};
  startDate = {year:0, month:0, day:0};
  torneos : Torneo[];
  equipos : Equipo[];
  closeResult: string;
  readOnly : boolean = false;
  page: number=1;
  maxSize:number=5;
  pageSize:number=10;
  @ViewChild('form') myForm: NgForm;
  constructor(private torneoService:TorneosService, private equiposService:EquiposService, private partidoService: PartidosService, private modalService: NgbModal , private alertService: AlertsService) { }

  ngOnInit() {
    this.torneoService.getTorneos().subscribe(torneos => this.torneos = torneos);
    this.equiposService.getEquipos().subscribe(equipos => this.equipos = equipos);
    this.readOnly=false;
  }


  selectTorneo(torneo:Torneo):void{
    this.selectedTorneo = torneo;
    this.partidos = [];
    this.partidoService.getPartidosFromTorneo(this.selectedTorneo.idTorneo).subscribe(partidos => this.partidos = partidos);
}

  selectEquipo1(equipo: Equipo): void {
    this.selectedEquipo1 = equipo;
  }

  selectEquipo2(equipo: Equipo): void {
    this.selectedEquipo2 = equipo;
  }

  cleanData(){
    //this.myForm.resetForm({});
    this.selectedEquipo1 = null;
    this.selectedEquipo2 = null;
    this.startTime = {hour: 0, minute: 0};
    this.startDate = {year:0, month:0, day:0};
    
  }

  getPartidos(): void {
    //this.partidos = this.partidoService.getPartidos();
    if (this.selectedTorneo){
      this.partidoService.getPartidosFromTorneo(this.selectedTorneo.idTorneo).subscribe(partidos => this.partidos = partidos);
    }
    
  }
  
  createNewPartido(content : any): void {
    let fechaPartido = new Date(this.startDate.year, this.startDate.month-1, this.startDate.day, this.startTime.hour, this.startTime.minute, 0, 0);
    if (this.selectedEquipo1 != null && this.selectedEquipo2 != null && isFuture(fechaPartido.valueOf())){//&& validar inicio
      
      let newPartido : Partido = {idPartido:0, equipo1 : this.selectedEquipo1, equipo2 : this.selectedEquipo2, torneo:this.selectedTorneo, inicio:fechaPartido, resultadoFinalEquipo1:-1, resultadoFinalEquipo2:-1, esFinal:false ,externalId: +this.newIdExterno};
      this.selectedPartido = newPartido;
      this.savePartido(null);
      this.selectedPartido = null;
      this.cleanData();
    }
    
  }

  formatDate(date:Date):string{
    return (format(date, 'YYYY-MM-DD h:mm:ss A'));
  }
  
  editPartido(partido: Partido, content : any):void{
    this.partidoService.getPartido(partido.idPartido).subscribe(partido=>this.selectedPartido=partido);
    this.readOnly=false;
    this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
  }
  
  deletePartido(partido: Partido):void{
    if (confirm('¿Está seguro que quiere eliminar el partido?')){
      this.partidoService.deletePartido(partido.idPartido).subscribe(result => this.getPartidos());
    }
  }
  
  viewPartido(partido: Partido, content : any):void{
    this.partidoService.getPartido(partido.idPartido).subscribe(partido=>this.selectedPartido=partido);
    this.readOnly=true;
    this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
  }
  
  onSubmit(c:any){
    this.savePartido(c);
  }
  savePartido(c:any): void {
    this.partidoService.savePartido(this.selectedPartido).subscribe(result => this.afterSave(c));
  }

  afterSave(c:any){
    this.getPartidos();
    if (c!=null){
      c('Saved');
    }
    this.alertService.alertSuccessMessage('Guardado exitosamente');
  }

  private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    setPage(page: number) {
      if (page < 1){// || page > this.pager.totalPages) {
          return;
      }
      if (isNaN(page)) {
          page = 1;
      }
      this.page = page;
  }
}
