import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { Torneo } from '../../../model/torneo';
import { TorneosService } from '../../../services/torneos.service';
import { AlertsService } from '../../../services/alerts.service';

@Component({
  selector: 'app-torneos',
  templateUrl: './torneos.component.html',
  styleUrls: ['./torneos.component.scss']
})
export class TorneosComponent implements OnInit {
  torneos : Torneo[];
  @Input() selectedTorneo : Torneo ;
  closeResult: string;
  readOnly : boolean = false;
  constructor(private torneoService: TorneosService, private modalService: NgbModal , private alertService: AlertsService) { }

  ngOnInit() {
    this.getTorneos();
    this.readOnly=false;
  }

  onSelect(torneo: Torneo): void {
    this.selectedTorneo = torneo;
  }

  getTorneos(): void {
    //this.torneos = this.torneoService.getTorneos();
    this.torneoService.getTorneos().subscribe(torneos => this.torneos = torneos);
  }
  
  createNewTorneo(content : any): void {
    let newTorneo : Torneo = {idTorneo:0, name:'', externalId: 0, active:true};
    this.selectedTorneo = newTorneo;
    this.readOnly=false;
   this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
  }
  
  editTorneo(torneo: Torneo, content : any):void{
    this.torneoService.getTorneo(torneo.idTorneo).subscribe(torneo=>this.loadTorneo(torneo));
    this.readOnly=false;
    this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
  }
  
  deleteTorneo(torneo: Torneo):void{
    if (confirm('¿Está seguro que quiere eliminar el torneo?')){
      this.torneoService.deleteTorneo(torneo.idTorneo).subscribe(result => this.getTorneos());
    }
  }
  
  viewTorneo(torneo: Torneo, content : any):void{
    this.torneoService.getTorneo(torneo.idTorneo).subscribe(torneo=>this.loadTorneo(torneo));
    this.readOnly=true;
    this.modalService.open(content, { size: 'lg', centered : true }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
  }

  loadTorneo(torneo:Torneo):void{
    this.selectedTorneo=torneo;
  }
  
  onSubmit(c:any){
    this.saveTorneo(c);
  }
  saveTorneo(c:any): void {
    this.torneoService.saveTorneo(this.selectedTorneo).subscribe(result => this.afterSave(c));
  }

  afterSave(c:any){
    this.getTorneos();
    c('Saved');
    this.alertService.alertSuccessMessage('Guardado exitosamente');
  }

  private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }
}
