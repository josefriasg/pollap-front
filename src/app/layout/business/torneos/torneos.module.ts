import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TorneosComponent } from './torneos.component';
import { TorneosRoutingModule } from './torneos-routing.module';
import { PageHeaderModule } from '../../../shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { TorneosService } from '../../../services/torneos.service';

@NgModule({
  declarations: [TorneosComponent],
  imports: [
    CommonModule, PageHeaderModule, FormsModule, NgbModule.forRoot(),
    TorneosRoutingModule
  ],providers: [
    TorneosService
  ]
})
export class TorneosModule { }
