import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'calendario', loadChildren: './business/calendario/calendario.module#CalendarioModule' },
            { path: 'pollas', loadChildren: './business/pollas/pollas.module#PollasModule' },
            { path: 'nueva', loadChildren: './business/crear-polla/crear-polla.module#CrearPollaModule' },
            { path: 'profile', loadChildren: './business/profile/profile.module#ProfileModule' },
            { path: 'comments', loadChildren: './business/comments/comments.module#CommentsModule' },
            { path: 'paises', loadChildren: './business/paises/paises.module#PaisesModule' },
            { path: 'equipos', loadChildren: './business/equipos/equipos.module#EquiposModule' },
            { path: 'torneos', loadChildren: './business/torneos/torneos.module#TorneosModule' },
            { path: 'partidos', loadChildren: './business/partidos/partidos.module#PartidosModule' },
            { path: 'deportes', loadChildren: './business/deportes/deportes.module#DeportesModule' },
            { path: 'marcadores', loadChildren: './business/marcadores/marcadores.module#MarcadoresModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
