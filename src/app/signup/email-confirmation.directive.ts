import { Directive } from '@angular/core';
import { AbstractControl, FormGroup, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';

export const emailConfirmationValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const username = control.get('username');
  const confUsername = control.get('confUsername');

  return username && confUsername && username.value != confUsername.value ? { 'emailsMismatch': true } : null;
};

@Directive({
  selector: '[appEmailConfirmation]',
  providers: [{ provide: NG_VALIDATORS, useExisting: EmailConfirmationDirective, multi: true }]
})
export class EmailConfirmationDirective implements Validator{

  constructor() { }

  validate(control: AbstractControl): ValidationErrors {
    return emailConfirmationValidator(control)
  }

}
