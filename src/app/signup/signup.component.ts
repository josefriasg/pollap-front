import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { Observable, of } from 'rxjs';
import { LoginService } from '../services/login.service';
import { AlertsService } from '../services/alerts.service';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {

    @Input() model: any = {};
    constructor(public router: Router, private loginService: LoginService, private alertService: AlertsService) {}

    ngOnInit() {}

    onSubmit() {
        let inpObj = <HTMLFormElement>document.getElementById("registrationForm");
        if (inpObj.checkValidity()) {//si valida pero no muestra los errores
            this.loginService.register(this.model.name, this.model.username, this.model.password).subscribe(isValid => {
                if (isValid) {
                    this.alertService.alertSuccessMessage('Registrado exitosamente, revise su email para completar el proceso');
                } else {
                    this.alertService.alertErrorMessage('Error durante registro');
                }
            });
        }
      }

    onRegister(){
        //alert('no hace nada');
    }
}
