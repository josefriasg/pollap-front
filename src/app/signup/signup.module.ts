import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import { LoginService } from '../services/login.service';

import { EmailConfirmationDirective } from './email-confirmation.directive';
import { AlertsModule } from '../shared/modules/alerts/alerts.module';
import { PasswordConfirmationModule } from '../shared/modules/password-confirmation/password-confirmation.module';

@NgModule({
  imports: [
    CommonModule, PasswordConfirmationModule,
    SignupRoutingModule, FormsModule, NgbModule.forRoot(), AlertsModule
  ],
  declarations: [SignupComponent, EmailConfirmationDirective],
  providers: [
      LoginService
    ]
})
export class SignupModule { }
