import { NgModule } from '@angular/core';
import { ActivationErrorComponent } from './activation-error.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
      path: '', component: ActivationErrorComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivationErrorRoutingModule { }
