import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivationErrorComponent } from './activation-error.component';
import { ActivationErrorRoutingModule } from './activation-error-routing.module';

@NgModule({
  declarations: [ActivationErrorComponent],
  imports: [
    CommonModule,
    ActivationErrorRoutingModule
  ]
})
export class ActivationErrorModule { }
