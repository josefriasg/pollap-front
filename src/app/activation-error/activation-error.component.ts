import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';

@Component({
  selector: 'app-activation-error',
  templateUrl: './activation-error.component.html',
  styleUrls: ['./activation-error.component.scss'],
  animations: [routerTransition()]
})
export class ActivationErrorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
