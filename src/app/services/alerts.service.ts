import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlertsService {
  private _success = new Subject<string>();
  private _error = new Subject<string>();

  constructor() { }

  getSuccessAlert(): Observable<any> {
    return this._success.asObservable();
  }
  getErrorAlert(): Observable<any> {
    return this._error.asObservable();
  }

  alertSuccessMessage(message: string) {
    this._success.next(message);
  }

  alertErrorMessage(message: string) {
    this._error.next(message);
  }

}
