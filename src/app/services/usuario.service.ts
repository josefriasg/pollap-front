import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Usuario } from '../model/usuario';
//import { CATEGORIAS } from './mock-categorias';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};
@Injectable()
export class UsuarioService {
  private usuarioURL = '/api/usuario';

  constructor(private http: HttpClient) { }

  getUsuarios(): Observable<Usuario[]> {
  return this.http.get<Usuario[]>(this.usuarioURL + '/list', httpOptions);
}

  getUsuario(idUsuario : number):Observable<Usuario>{
    return this.http.get<Usuario>(this.usuarioURL + '/get/'+idUsuario);
  }

  saveUsuario(usuario:Usuario): Observable<any> {
    return this.http.put(this.usuarioURL + '/save', usuario, httpOptions);
  }
  
  deleteUsuario(idUsuario: number): Observable<any> {
    return this.http.delete(this.usuarioURL + '/delete/'+idUsuario, httpOptions);
  }

  uploadImage(image:any): Observable<any> {
    return this.http.post(this.usuarioURL + '/upload/picture', image);
  }

  getUserWithProfilePicture(username:string):Observable<Usuario>{
    return this.http.get<Usuario>(this.usuarioURL + '/download/picture/'+username);
  }

  getCurUserWithProfilePicture():Observable<Usuario>{
    return this.http.get<Usuario>(this.usuarioURL + '/download/picture');
  }

  loadNotification():Observable<boolean>{
    return this.http.get<boolean>(this.usuarioURL +'/get/notification');
  }

  updateNotification(notification:boolean):Observable<boolean>{
    return this.http.get<boolean>(this.usuarioURL +'/update/notification/'+notification);
  }
}
