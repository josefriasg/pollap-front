import { TestBed } from '@angular/core/testing';

import { DeportesService } from './deportes.service';

describe('DeportesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeportesService = TestBed.get(DeportesService);
    expect(service).toBeTruthy();
  });
});
