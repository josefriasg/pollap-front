import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Comentario } from '../model/comentario';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};
@Injectable()
export class CommentsService {
  private comentarioURL = '/api/comentario';

  constructor(private http: HttpClient) { }

  getComentarios(): Observable<Comentario[]> {
  return this.http.get<Comentario[]>(this.comentarioURL + '/list', httpOptions);
}

  saveComentario(comentario:string): Observable<any> {
    return this.http.put(this.comentarioURL + '/save', comentario, httpOptions);
  }
}