import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Equipo } from '../model/equipo';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};
@Injectable()
export class EquiposService {
  private equipoURL = '/api/equipo';

  constructor(private http: HttpClient) { }

  getEquipos(): Observable<Equipo[]> {
  return this.http.get<Equipo[]>(this.equipoURL + '/list', httpOptions);
}

getEquiposFromPais(idPais : number): Observable<Equipo[]> {
  return this.http.get<Equipo[]>(this.equipoURL + '/list/'+idPais, httpOptions);
}

  getEquipo(idEquipo : number):Observable<Equipo>{
    return this.http.get<Equipo>(this.equipoURL + '/get/'+idEquipo);
  }

  saveEquipo(equipo:Equipo): Observable<any> {
    return this.http.put(this.equipoURL + '/save', equipo, httpOptions);
  }
  
  deleteEquipo(idEquipo: number): Observable<any> {
    return this.http.delete(this.equipoURL + '/delete/'+idEquipo, httpOptions);
  }
}
