import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Polla } from '../model/polla';
import { Posicion } from '../model/posicion';
import { Partido } from '../model/partido';
import { Prediccion } from '../model/prediccion';
import { PrediccionUsuario } from '../model/prediccionUsuario';
import { EventoCalendario } from '../model/eventoCalendario';
import { ResultadoOperacion } from '../model/resultadoOperacion';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};
@Injectable()
export class PollasService {
  private pollaURL = '/api/polla';

  constructor(private http: HttpClient) { }

  getPollas(): Observable<Polla[]> {
  return this.http.get<Polla[]>(this.pollaURL + '/list', httpOptions);
}

  getPolla(idPolla : number):Observable<Polla>{
    return this.http.get<Polla>(this.pollaURL + '/get/'+idPolla);
  }

  getPollaBasic(idPolla : number):Observable<Polla>{
    return this.http.get<Polla>(this.pollaURL + '/getBasic/'+idPolla);
  }

  getPollaUniqueCode(idPolla : number):Observable<string>{
    return this.http.get<string>(this.pollaURL + '/getPollaUniqueCode/'+idPolla);
  }

  savePolla(polla:Polla): Observable<any> {
    return this.http.put(this.pollaURL + '/save', polla, httpOptions);
  }
  
  deletePolla(idPolla: number): Observable<any> {
    return this.http.delete(this.pollaURL + '/delete/'+idPolla, httpOptions);
  }

  getPosiciones(idPolla : number): Observable<Posicion[]> {
    return this.http.get<Posicion[]>(this.pollaURL + '/posiciones/'+idPolla, httpOptions);
  }

  getPosicionesWithPicture(idPolla : number): Observable<Posicion[]> {
    return this.http.get<Posicion[]>(this.pollaURL + '/posiciones/picture/'+idPolla, httpOptions);
  }

  joinPolla(codigoPolla:string): Observable<any> {
    return this.http.put(this.pollaURL + '/join/'+codigoPolla, codigoPolla, httpOptions);
  }

  getPrediccionPolla(idPolla : number, action:string): Observable<Prediccion[]> {
    return this.http.get<Prediccion[]>(this.pollaURL + '/predicciones/'+action+'/'+idPolla, httpOptions);
  }

  getPrediccionesPollas(viewDate:Date): Observable<EventoCalendario[]>{
    return this.http.get<EventoCalendario[]>(this.pollaURL + '/predicciones/all/'+viewDate, httpOptions);
  }

  getPrediccionPollaFromUser(idPolla : number, idUser : number): Observable<Prediccion[]> {
    return this.http.get<Prediccion[]>(this.pollaURL + '/predicciones/user/'+idPolla+"/"+idUser, httpOptions);
  }

  getPrediccionesPartidoPolla(idPartido:number, idPolla:number): Observable<PrediccionUsuario[]>{
    return this.http.get<PrediccionUsuario[]>(this.pollaURL + '/predicciones/partido/polla/'+idPartido+"/"+idPolla, httpOptions);
  }

  savePrediccionPolla(predicciones:Prediccion[], idPolla : number): Observable<any> {
    return this.http.put(this.pollaURL + '/save/predicciones/'+idPolla, predicciones, httpOptions);
  }

  getCountPollas():Observable<number>{
    return this.http.get<number>(this.pollaURL + '/count', httpOptions);
  }

  getPartidosEmpezadosFromPolla(idPolla:number):Observable<Partido[]>{
    return this.http.get<Partido[]>(this.pollaURL + '/partidos/'+idPolla, httpOptions);
  }
}
