import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Deporte } from '../model/deporte';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};
@Injectable()
export class DeportesService {
  private deporteURL = '/api/deporte';

  constructor(private http: HttpClient) { }

  getDeportes(): Observable<Deporte[]> {
  return this.http.get<Deporte[]>(this.deporteURL + '/list', httpOptions);
}

  getDeporte(idDeporte : number):Observable<Deporte>{
    return this.http.get<Deporte>(this.deporteURL + '/get/'+idDeporte);
  }

  saveDeporte(deporte:Deporte): Observable<any> {
    return this.http.put(this.deporteURL + '/save', deporte, httpOptions);
  }
  
  deleteDeporte(idDeporte: number): Observable<any> {
    return this.http.delete(this.deporteURL + '/delete/'+idDeporte, httpOptions);
  }
}
