import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Partido } from '../model/partido';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};
@Injectable()
export class PartidosService {
  private partidoURL = '/api/partido';

  constructor(private http: HttpClient) { }

  getPartidos(): Observable<Partido[]> {
  return this.http.get<Partido[]>(this.partidoURL + '/list', httpOptions);
}

getPartidosFromTorneo(idTorneo : number): Observable<Partido[]> {
  return this.http.get<Partido[]>(this.partidoURL + '/list/'+idTorneo, httpOptions);
}

  getPartido(idPartido : number):Observable<Partido>{
    return this.http.get<Partido>(this.partidoURL + '/get/'+idPartido);
  }

  savePartido(partido:Partido): Observable<any> {
    return this.http.put(this.partidoURL + '/save', partido, httpOptions);
  }

  saveMarcadores(partidos:Partido[]): Observable<any> {
    return this.http.put(this.partidoURL + '/save/marcadores', partidos, httpOptions);
  }
  
  deletePartido(idPartido: number): Observable<any> {
    return this.http.delete(this.partidoURL + '/delete/'+idPartido, httpOptions);
  }
}
