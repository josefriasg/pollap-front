import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Pais } from '../model/pais';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};
@Injectable()
export class PaisesService {
  private paisURL = '/api/pais';

  constructor(private http: HttpClient) { }

  getPaises(): Observable<Pais[]> {
  return this.http.get<Pais[]>(this.paisURL + '/list', httpOptions);
}

  getPais(idPais : number):Observable<Pais>{
    return this.http.get<Pais>(this.paisURL + '/get/'+idPais);
  }

  savePais(pais:Pais): Observable<any> {
    return this.http.put(this.paisURL + '/save', pais, httpOptions);
  }
  
  deletePais(idPais: number): Observable<any> {
    return this.http.delete(this.paisURL + '/delete/'+idPais, httpOptions);
  }
}
