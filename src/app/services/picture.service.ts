import { Injectable } from '@angular/core';
import { Subject, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PictureService {

  _pictureSource = new Subject<string>();
  picture$ = this._pictureSource.asObservable();

  constructor() { }

  updatePicture(picture:string){
    localStorage.setItem('picture', picture);
    this._pictureSource.next(picture);
  }

}
