import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Torneo } from '../model/torneo';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};
@Injectable()
export class TorneosService {
  private torneoURL = '/api/torneo';

  constructor(private http: HttpClient) { }

  getTorneos(): Observable<Torneo[]> {
  return this.http.get<Torneo[]>(this.torneoURL + '/list', httpOptions);
}

getActiveTorneos(): Observable<Torneo[]> {
  return this.http.get<Torneo[]>(this.torneoURL + '/list/active', httpOptions);
}

  getTorneo(idTorneo : number):Observable<Torneo>{
    return this.http.get<Torneo>(this.torneoURL + '/get/'+idTorneo);
  }

  saveTorneo(torneo:Torneo): Observable<any> {
    return this.http.put(this.torneoURL + '/save', torneo, httpOptions);
  }
  
  deleteTorneo(idTorneo: number): Observable<any> {
    return this.http.delete(this.torneoURL + '/delete/'+idTorneo, httpOptions);
  }
}
