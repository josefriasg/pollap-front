import { TestBed } from '@angular/core/testing';

import { PollasService } from './pollas.service';

describe('PollasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PollasService = TestBed.get(PollasService);
    expect(service).toBeTruthy();
  });
});
