import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};

@Injectable()
export class LoginService {
  private loginURL = '/api/security';

  constructor(private http: HttpClient) { }

  login(username, password): Observable<any> {
  return this.http.post<Observable<boolean>>(this.loginURL + '/signon', {
    email: username,
    password: password
});
}

sso(ticket): Observable<any> {
  return this.http.post<Observable<boolean>>(this.loginURL + '/sso/'+ticket, {});
}

register(name, username, password): Observable<any> {
  return this.http.post<Observable<boolean>>(this.loginURL + '/register', {
    name: name,
    email: username,
    password: password
});
}

logout(): Observable<any> {
  return this.http.post<Observable<boolean>>(this.loginURL + '/logout', {});
}

changePassword(password:string, newPassword:string): Observable<any> {
  return this.http.post<Observable<boolean>>(this.loginURL + '/changePassword', {
    password: password,
    newPassword: newPassword
});
}

resetPassword(email:string): Observable<any> {
  return this.http.post<Observable<boolean>>(this.loginURL + '/resetPassword', {
    email: email
});
}

}
