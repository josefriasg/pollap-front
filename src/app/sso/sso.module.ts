import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SsoComponent } from './sso.component';
import { SsoRoutingModule } from './/sso-routing.module';
import { LoginService } from '../services/login.service';

@NgModule({
  imports: [
    CommonModule,
    SsoRoutingModule
  ],
  declarations: [SsoComponent],
  providers: [
    LoginService
  ]
})
export class SsoModule { }
