import { Component, OnInit, Inject } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';

@Component({
  selector: 'app-sso',
  templateUrl: './sso.component.html',
  styleUrls: ['./sso.component.scss'],
  animations: [routerTransition()]
})
export class SsoComponent implements OnInit {

  constructor(public router: Router, private loginService: LoginService) { }

  ngOnInit() {//sera que solo hace el on init una vez?
    sessionStorage.removeItem('data');
    localStorage.removeItem('isLoggedin');
    let ticket = sessionStorage.getItem('ticket');
    this.loginService.sso(ticket).subscribe(data => {
      if (data != null) {
        if (data.token){
            sessionStorage.setItem(
            'data',JSON.stringify(data)
            //btoa(this.model.username + ':' + this.model.password)
            );
            localStorage.setItem('isLoggedin', 'true');
            this.router.navigate(['tomar-encuestas']);
        }else{
          sessionStorage.removeItem('data');
          localStorage.removeItem('isLoggedin');
          this.router.navigate(['login']);
        }
    } else {
          sessionStorage.removeItem('data');
          localStorage.removeItem('isLoggedin');
          this.router.navigate(['login']);
      }
  });
  }

}
